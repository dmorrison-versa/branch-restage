#!/usr/bin/env python

# this is the re-staging script for branches that are on the 16.1R2 or 20.2 software
# Tested on: 16.1r2S11, 20.2.3
# version 1.4
# Changelog:
#   Version 1.4:
#       - changed 20.2.x version to use the erase running-config via cli rather than shell script call
#   Version 1.3:
#       - suppressed the error message when erasing the running-configuration
#       - added additional messaging at the end of phase1
#   Version 1.2:
#      - No change, upreved to match corresponding 21.x.x release
#   Version 1.1:
#      - Fix for vsh status outputting a subjugated message
#   Version 1.0:
#      - Initial release


import os
import logging
import datetime
from pwd import getpwnam
import grp
import sys
import argparse
import pickle
import base64
from tempfile import NamedTemporaryFile
import json
import time
import imp


staging_script_path = '/opt/versa/scripts/staging.py'
staging_script = imp.load_source('staging', staging_script_path)


class OwnedFileHandler(logging.FileHandler):
    """Create a log file owned by user:group"""

    def __init__(self, filename, mode='a', encoding=None, delay=False, uid=None, gid=None):
        logging.FileHandler.__init__(self, filename, mode=mode, encoding=encoding, delay=delay)
        if os.path.exists(filename) and uid and gid:
            os.chown(filename, uid, gid)


def deserialize(base64_string):
    """Return an object from a pickled string"""
    base64_bytes = base64_string.encode("utf-8")
    pickle_bytes = base64.b64decode(base64_bytes)
    obj = pickle.loads(pickle_bytes)
    return (obj)


def serialize(obj):
    """Return a string representation of this object"""
    pickle_string = pickle.dumps(obj)
    base64_bytes = base64.b64encode(pickle_string)
    base64_string = base64_bytes.decode('utf-8')
    return (base64_string)


def execute_process(command):
    result = os.popen('/bin/bash -c ' + "\'" + command + "\'").read()
    log("result = {}".format(result), terminal=False)
    return result


def execute_cli_command(command):
    # NOTE: __CLI is polymorphically defined in the base classes as self._VersaDevice__CLI
    __CLI = '/opt/versa/confd/bin/confd_cli -u versa'
    command = """\" {}\"""".format(command)
    command = __CLI + " <<< " + command
    log("executing cli command: {}".format(command), terminal=False)
    result = os.popen('/bin/bash -c ' + "\'" + command + "\'").read()
    inconsistent_db_message = "result: \n--- WARNING ------------------------------------------------------\nRunning db may be inconsistent. Enter configuration mode and\nload a saved configuration.\n------------------------------------------------------------------\n"
    result = result.replace(inconsistent_db_message, "")
    log("result: {}".format(result), terminal=False)
    return result


def schedule_command(command, delay_minutes=0):
    """Restart Versa Services with a delay of X minutes"""
    log('scheduling {} for execution in {} minutes'.format(command, delay_minutes), terminal=False)
    # NOTE: __ssh_banner_path is polymorphically defined in the base classes as self._VersaDevice__ssh_banner_path
    f = NamedTemporaryFile(delete=False)
    f.write(command.encode())
    f.close()
    command = "sudo at -f {} now + {} min 2>&1".format(f.name, delay_minutes)
    output = execute_process(command)
    if "job" in output:
        log("command {} successfully scheduled at: {}".format(command, delay_minutes), terminal=False)
        ret = True
    else:
        log("command {} not successfully scheduled".format(command), terminal=False)
        ret = False
    os.unlink(f.name)
    return ret


def create_rollback():
    rollback_name = "restage_" + execution_time
    log('creating system rollback: {}'.format(rollback_name))
    command = 'request system create-snapshot description {} no-confirm '.format(rollback_name)
    result = execute_cli_command(command)
    if 'System state saved' in result:
        log("successfully created rollback")
        return True
    log("creating rollback failed")
    return False


def get_rollback_timestamp(execution_time):
    rollback_desc = "restage_" + execution_time
    snapshots_json = execute_cli_command('show system snapshots | display json')
    snapshots = json.loads(snapshots_json)
    snapshots = snapshots['data']['system:system']['snapshots']
    timestamp = None
    for s in snapshots:
        if s['description'] == rollback_desc:
            timestamp = s['timestamp']
            break
    return timestamp


def rollback(timestamp):
    timestamp = get_rollback_timestamp(timestamp)
    command = 'request system rollback no-confirm to {}'.format(timestamp)
    result = execute_cli_command(command)


def is_ptvi_up():
    command = 'show interfaces brief | tab | grep ptvi'
    result = execute_cli_command(command)
    for r in result.split('\n'):
        if "up    up" in r and '-Control-VR' in r:
            return True
    return False


def rollback_if_needed(timestamp, check_function, pass_message, fail_message):
    for x in range(60):
        if check_function():
            log(pass_message, terminal=False)
            return True
        time.sleep(10)
    log(fail_message, terminal=True)
    rollback(timestamp)
    return False

def get_package_version():
    package_info_path = "/opt/versa/etc/package_info.in"
    with open(package_info_path) as f:
        lines = f.readlines()
        version = {}
        for line in lines:
            if "ver_major" in line or "ver_minor" in line or "ver_service" in line:
                token_list = line.replace("\'", "").replace("\n", "").replace("\t", "").replace(" ", "").replace(",",
                                                                                                                 "").split(
                    "=")
                version[token_list[0]] = token_list[1]
        if len(version) == 3:
            return "{}.{}.{}".format(version["ver_major"], version["ver_minor"], version["ver_service"])

        # legacy version
        for line in package_info:
            if 'versa_pkg' in line:
                pkg_list = line.split('-')
                return pkg_list[len(pkg_list) - 1].split('\'')[0]

        f.close()
        return version


def reset_config():
    log('erasing device configuration')
    release = get_package_version()
    log("release = {}".format(release), terminal=True)
    if "20.2" in release:
        command = "request erase running-config"
        result = execute_cli_command(command)
    else:
        command = "/opt/versa/scripts/cdbclear.sh 2>/dev/null"
        result = execute_process(command)
    log(result, terminal=True)


def get_confirmation():
    cont_message_1 = "this script will reset the device and restage it using the passed paramerters.\n"
    cont_message_1 = "{}this will cause a service outage for the duration of the script\n".format(cont_message_1)
    cont_message_1 = "{}do you want to continue (y/n):  ".format(cont_message_1)
    log(cont_message_1, terminal=False)

    cont = raw_input(cont_message_1).rstrip()
    while (cont.lower() != 'y' and cont.lower() != 'n'):
        cont = raw_input(cont_message_1)
    if cont.lower() == 'n':
        log("received n, exiting")
        sys.exit()
    cont_message_2 = "Please confirm one last time, this will reset the device and restage using the pass parameters (y/n): "
    log(cont_message_2, terminal=False)
    cont = raw_input(cont_message_2).rstrip()
    while (cont.lower() != 'y' and cont.lower() != 'n'):
        cont = raw_input(cont_message_2)
    if cont.lower() == 'n':
        log("received n, exiting")
        sys.exit()


def setup_logging(execution_time):
    log_filename = '{}_{}.log'.format("restage", execution_time)
    logging.basicConfig(filename=log_filename,
                        level=logging.INFO,
                        format='%(asctime)s - %(levelname)s - %(message)s')
    owned_file_handler = OwnedFileHandler(log_filename, mode='a+',
                                          uid=getpwnam('versa').pw_uid,
                                          gid=grp.getgrnam('versa')[2])
    log = logging.getLogger()
    log.handlers.pop()
    log.addHandler(owned_file_handler)
    log.setLevel(logging.INFO)


def log(message, terminal=True):
    logging.info(message)
    if terminal:
        print(message)


def is_versa_services_running():
    status = execute_process("service versa-flexvnf status").split('\n')
    for s in status:
        if s and s.startswith("versa-") and not 'is Running' in s:
            log("versa services not running", terminal=False)
            return False
    log("all services running", terminal=False)
    return True


def check_versa_services():
    for x in range(60):
        if is_versa_services_running():
            log("versa services running, continuing")
            return True
        time.sleep(10)
    log("10 minutes have passed without all versa services starting, attempting restart", terminal=True)
    status = execute_process("service versa-flexvnf restart")
    return False


def is_netconf_connected():
    status = execute_process("netstat -a | grep 2022 | grep ESTABLISHED")
    if status:
        log("netconf connected", terminal=False)
        return True
    else:
        log("netconf not connected", terminal=False)
        return False


def passtwo(p2_args):
    """
    :param p2_args:
    :return:
    1. use the staging script to check if the passed parameters are valid, if not abort
    2. use the staging script to generate the configuration
    3. use the staging script to generate the serial number
    4. use the staging script to check for confd
    5. use the staging script to load the staging configuration
    6. schedule passthree to rollback in the case of an error
    """
    log("checking prerequisites")
    staging_script.check_prereq()
    log("generating configurations")
    staging_script.generate_cfg(arg_data["args"])
    log("generating serial number")
    staging_script.generate_serial(args.serial_number)
    if 'check_confd_up' in dir(staging_script):
        log("checking to see if the confd service is up")
        staging_script.check_confd_up()
    try:
        # this is for the pyinstaller
        x = sys._MEIPASS
        script_path = sys.executable
    except AttributeError:
        # this is for the standalone script
        script_path = "{} {}".format(sys.executable, os.path.realpath(__file__))
    schedule_command("{} --passthree {}".format(script_path, p2_args), delay_minutes=5)
    log("loading configuration")
    staging_script.load_cfg()


def passone():
    """
    passone:
    1. Check to see if versa services are running, exit if not
    2. Check to see if netconf is up to the director
    3. encode the  parameters for use in passtwo
    4.a. schedule passtwo to run using the AT command
    4.b. if passtwo is successfully scheduled then create a rollback
    4.c. if passtwo is successfully scheduled erase the running configuration
    :return:
    """
    global execution_time, arg_data, x
    staging_script.validate_args(args)
    execution_time = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
    setup_logging(execution_time)
    if not is_versa_services_running():
        log("Versa services are not running, exiting")
        sys.exit(-1)
    if not is_netconf_connected():
        log("netconf to director is not connected, exiting")
        sys.exit(-1)
    log("passed args: {}".format(args), terminal=False)
    get_confirmation()
    arg_data = {}
    arg_data['execution_time'] = execution_time
    arg_data['args'] = args
    serialized_data = serialize(arg_data)
    try:
        # this is for the pyinstaller
        x = sys._MEIPASS
        script_path = sys.executable
    except AttributeError:
        # this is for the standalone script
        script_path = "{} {}".format(sys.executable, os.path.realpath(__file__))
    if schedule_command("{} --passtwo {}".format(script_path, serialized_data),
                        delay_minutes=5):

        rollback_created = create_rollback()
        if rollback_created:
            reset_config()
            log("pass one successfully completed and staging.py scheduled for 5 minutes\nthis script will exit now\nplease expect the device to reboot after successfully running the staging script", terminal=True)

if __name__ == "__main__":
    if not os.geteuid() == 0:
        print("This script must be ran with root privileges, please run as sudo.  Exiting!")
        sys.exit(1)

    parser = staging_script.add_args()
    parser.description = 'erase device configuration and restage using pass parameters'
    parser.add_argument('--passtwo', required=False, help=argparse.SUPPRESS)
    parser.add_argument('--passthree', required=False, help=argparse.SUPPRESS)
    args = parser.parse_args()

    if not args.passtwo and not args.passthree:
        # no args are given, execute passone
        passone()
    else:
        if args.passtwo:
            # passtwo logs are given, execute passtwo
            log("we are in pass two!!!!", terminal=False)
            arg_data = deserialize(args.passtwo)
        else:
            # passthress logs are give, execute passthree
            log("we are in pass three!!!!", terminal=False)
            arg_data = deserialize(args.passthree)

        # common code for passtwo and passthree
        # setup logging and write our passed arguments
        setup_logging(arg_data['execution_time'])
        log("arg_data: {}".format(arg_data))

        # wait for versa services to be running, blow chunks if they aren't
        log("waiting for versa services", terminal=False)
        services_running = False
        for x in range(3):
            if check_versa_services():
                services_running = True
                break
        if not services_running:
            log("extreme failure, Versa services will not start")
            sys.exit(-1)

        if args.passtwo:
            # execute passtwo only if the passtwo parameter is passed
            passtwo(args.passtwo)

        # the following two checks execute in both passtwo and passthree
        # note: if passtwo is successful then these checks will never happen due to the reset that occurs during staging

        # check to see if there is a ptvi connection, if one does not come up then rollback to the snapshot taken in passone
        log("checking for ptvi connection")
        rollback_if_needed(arg_data['execution_time'],
                           is_ptvi_up,
                           "ptvi interfaces are up, rollback cancelled",
                           "10 minutes have passed without connection to controllers, rolling back")
        log("checking for netconf connection")
        rollback_if_needed(arg_data['execution_time'],
                           is_netconf_connected,
                           "netconf connection with director is up, rollback cancelled",
                           "10 minutes have passed without netconf connection to director, rolling back")