# Description
This tool is intended to help in migrating a branch from one system to another by allowing for a remote reconfiguration without the need for an onsight technitian to run the staging script.  It works with the staging.py script to schedule and erase configuration and to restage the device ~5 minutes after execution.
#High Level Flow

Phase 1 (Initial run):

1.	Check the passed arguments to ensure that they are compatible with the staging.py script
2.	Schedule the script run in phase 2 for 5 minutes in the future
3.	Create a snapshot on the device
a.	If snapshot fails exit
4.	Run the “request erase running configuration command”
    a.	If erase command fails exit
5.	Exit

Phase 2 (restage run):
1.	Wake up, get arguments from phase 1 script
2.	Loop for 10 minutes, Check for Versa services to be running
    a.	If versa services are running within the 10 minutes, move to (3)
    b.	If after 10 minutes versa services are not running, restart all versa services
        i.	Retry this 2.b 3 times
3.	Schedule phase 3
4.	Execute staging script to run again in 5 minutes
5.	Loop for 10 minutes, checking for establishment of ptvi interfaces
    a.	If succeeds, goto 5
    b.	If fails, initiate rollback to snapshot
6.	Loop for 10 minutes, checking for establishment of Netconf
    a.	If succeeds, goto 5
    b.	If fails, initiate rollback to snapshot
7.	Exit

Phase 3 (verification):
1.	Loop for 10 minutes, Check for Versa services to be running
    a.	If versa services are running within the 10 minutes, move to (3)
    b.	If after 10 minutes versa services are not running, restart all versa services
        i.	Retry this 2.b 3 times
2.	Loop for 10 minutes, checking for establishment of ptvi interfaces
    a.	If succeeds, goto 5
    b.	If fails, initiate rollback to snapshot
3.	Loop for 10 minutes, checking for establishment of Netconf
    a.	If succeeds, goto 5
    b.	If fails, initiate rollback to snapshot
4.	Exit

#Execution
1. copy the appropriate script (based on FlexVNF version) to the branch device
2. run the script using sudo using the standard staging options

for help run **$ sudo staging.py -h**

Example:

sudo ./restage.py -n DM002 -w 0 -d -c 54.212.1.242 -r Controller-1-staging@VERSA.com -l chicago-auth@versa.com


Note: if using 21.1.x the staging.py script included in the 21.1.x directory needs to be copied to /opt/versa/scripts/staging.py 
